import React from 'react';
import { Counter } from './components/Counter';


const App = () => {
	return (
		<div className='container bg-white p-4 mt-5'>
			<h1>My Counter</h1>
			<Counter />
		</div>
	);
};

export default App;